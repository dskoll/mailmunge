#
# SPDX-License-Identifier: GPL-2.0-only
#
use strict;
use warnings;
use lib '.';
use Test::More;
use Test::Mailmunge::RegressionUtils;

my $mta = `mm-mx-ctrl mta 2>/dev/null`;
chomp($mta);

if (server_running_postfix()) {
        is($mta, 'postfix', 'Correctly detected Postfix MTA');
} else {
        is($mta, 'sendmail', 'Correctly detected Sendmail MTA');
}

done_testing;
