#
# SPDX-License-Identifier: GPL-2.0-only
#
use strict;
use warnings;

use lib './lib';

use Test::More;
use Test::Deep;

use Test::Mailmunge::Tmpdir;
use Test::Mailmunge::Utils;
use MIME::Parser;

my $t = Test::Mailmunge::Tmpdir->new();
my $d = $t->{dir};

start_multiplexor($d, 't/filters/test-filter.pl');

my $msg_dir = $d . '/mm-foobar';
mkdir($msg_dir, 0755);

my $ctx = parse_and_copy_msg($msg_dir, 't/data/double-boundary/inputmsg');
ok($ctx->mime_entity, 'Successfully parsed sample message');

# Set some fields
$ctx->recipients(['<bar@example.com>']);

write_commands_file($msg_dir, $ctx);
my $ans = mm_mx_ctrl($d, 'scan', 'NOQUEUE', $msg_dir);

is($ans, 'ok', 'Filter succeeded');
if (MIME::Parser->can('ambiguous_content')) {
        is(`cat t/data/double-boundary/RESULTS`, `cat $msg_dir/RESULTS 2>/dev/null`, 'Got expected RESULTS file');
} else {
        ok(1, 'MIME::Parser does not support ambiguous_content method');
        is("F\n", `cat $msg_dir/RESULTS 2>/dev/null`, 'Got expected RESULTS file');
}

END: {
        stop_multiplexor($d);
}

done_testing;

1;
