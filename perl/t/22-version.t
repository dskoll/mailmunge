#
# SPDX-License-Identifier: GPL-2.0-only
#
use strict;
use warnings;

use lib './lib';

use Test::More;
use Test::Deep;

use Test::Mailmunge::Tmpdir;
use Test::Mailmunge::Utils;

my $t = Test::Mailmunge::Tmpdir->new();
my $d = $t->{dir};

start_multiplexor($d, 't/filters/test-filter.pl');

my $ans;

$ans = mm_mx_ctrl($d, 'filterversion');
is ($ans, 'Testy_Version_Haha', 'Got expected filter version');

$ans = mm_mx_ctrl($d, 'rawversion');
is ($ans, Mailmunge::Filter::mailmunge_version(), 'Got expected software version');
END: {
        stop_multiplexor($d);
}

done_testing;
