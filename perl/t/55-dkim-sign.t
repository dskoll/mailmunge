#
# SPDX-License-Identifier: GPL-2.0-only
#
use strict;
use warnings;

use lib './lib';

use Test::More;
use Test::Deep;

use Test::Mailmunge::Tmpdir;
use Test::Mailmunge::Utils;

my $t = Test::Mailmunge::Tmpdir->new();
my $d = $t->{dir};

start_multiplexor($d, 't/filters/test-filter.pl');

my $msg_dir = $d . '/mm-dkim';
mkdir($msg_dir, 0755);

my $ctx = parse_and_copy_msg($msg_dir, 't/data/for-dkim/for-dkim.msg', 'dkim_sign');

ok($ctx->mime_entity, 'Successfully parsed sample message');
$ctx->recipients(['<bar@example.com>']);
write_commands_file($msg_dir, $ctx);
my $ans = mm_mx_ctrl($d, 'scan', 'qIa12345', $msg_dir);
is($ans, 'ok', 'Filter succeeded');

is(`cat $msg_dir/RESULTS`, `cat t/data/for-dkim/RESULTS`, 'Got expected RESULTS file');

system('rm', '-rf', $msg_dir);
mkdir($msg_dir, 0755);

# DKIM-sign early; should get a warning in the logs
$ctx = parse_and_copy_msg($msg_dir, 't/data/for-dkim/for-dkim.msg', 'dkim_sign_early boilerplate_one_start');

ok($ctx->mime_entity, 'Successfully parsed sample message');
$ctx->recipients(['<bar@example.com>']);
write_commands_file($msg_dir, $ctx);
$ans = mm_mx_ctrl($d, 'scan', 'qIa12345', $msg_dir);
is($ans, 'ok', 'Filter succeeded');

my $logs = get_logs($msg_dir);

my $first_log = $logs->[0];
cmp_deeply($first_log, [undef, 'warning', 'Mailmunge::Action::DKIMSigner::add_dkim_signature called outside of filter_wrapup: DKIM signature may be invalid'], 'Got expected warning in logs if we cakk dkim_sign outside of filter_wrapup');

system('rm', '-rf', $msg_dir);
mkdir($msg_dir, 0755);
# Now sign a modified message
$ctx = parse_and_copy_msg($msg_dir, 't/data/for-dkim/for-dkim.msg', 'dkim_sign boilerplate_one_start');

ok($ctx->mime_entity, 'Successfully parsed sample message');
$ctx->recipients(['<bar@example.com>']);
write_commands_file($msg_dir, $ctx);
$ans = mm_mx_ctrl($d, 'scan', 'qIa12345', $msg_dir);
is($ans, 'ok', 'Filter succeeded');

is(`cat $msg_dir/RESULTS`, `cat t/data/for-dkim/RESULTS-MODIFIED`, 'Got expected RESULTS file');

system('rm', '-rf', $msg_dir);
mkdir($msg_dir, 0755);

# Try one where we change the subject
$ctx = parse_and_copy_msg($msg_dir, 't/data/for-dkim/for-dkim.msg', 'dkim_sign chgsub haha');

ok($ctx->mime_entity, 'Successfully parsed sample message');
$ctx->recipients(['<bar@example.com>']);
write_commands_file($msg_dir, $ctx);
$ans = mm_mx_ctrl($d, 'scan', 'qIa12345', $msg_dir);
is($ans, 'ok', 'Filter succeeded');

is(`cat $msg_dir/RESULTS`, `cat t/data/for-dkim/RESULTS-CHGSUB`, 'Got expected RESULTS file');

is(`cat $msg_dir/NEWHEADERS`, `cat t/data/for-dkim/NEWHEADERS`, 'Got expected NEWHEADERS file');

system('rm', '-rf', $msg_dir);

 END: {
        stop_multiplexor($d);
}

done_testing;

1;
