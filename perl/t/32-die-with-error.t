#
# SPDX-License-Identifier: GPL-2.0-only
#
use strict;
use warnings;

use lib './lib';

use Test::More;
use Test::Deep;

use Test::Mailmunge::Tmpdir;
use Test::Mailmunge::Utils;

my $t = Test::Mailmunge::Tmpdir->new();
my $d = $t->{dir};

start_multiplexor($d, 't/filters/test-filter.pl');

my $msg_dir = $d . '/mm-foobar';
mkdir($msg_dir, 0755);

my $ctx = parse_and_copy_msg($msg_dir, 't/data/die-with-error/inputmsg');
ok($ctx->mime_entity, 'Successfully parsed sample message');

# Set some fields
$ctx->recipients(['<bar@example.com>']);

write_commands_file($msg_dir, $ctx);
my $ans = mm_mx_ctrl($d, 'scan', 'NOQUEUE', $msg_dir);

is($ans, 'ERR No response from worker', 'Filter failed');
is(`cat t/data/die-with-error/RESULTS`, `cat $msg_dir/RESULTS 2>/dev/null`, 'Got expected RESULTS file');

END: {
        stop_multiplexor($d);
}

done_testing;

1;
