#
# SPDX-License-Identifier: GPL-2.0-only
#
package Mailmunge::Action::DKIMSigner;
use strict;
use warnings;

use base qw(Mailmunge::Action);

use IO::File;

sub add_dkim_signature
{
        my ($self, $ctx, $signer) = @_;

        my $wd = $ctx->cwd;

        my $fh;

        my $filter = $self->filter;

        my $consume;
        my $done_one = 0;

        if (!$ctx->in_filter_wrapup) {
                $ctx->log('warning', 'Mailmunge::Action::DKIMSigner::add_dkim_signature called outside of filter_wrapup: DKIM signature may be invalid');
        }
        $self->filter->push_tag($ctx, "DKIM-signing mail");

        # If message has been altered, sign the altered message;
        # If headers have been altered, sign the altered headers
        # otherwise; sign original
        if (-f $wd . '/' . $filter->_newbody()) {
                if (-f $self->filter->_newheaders) {
                        $consume = [$filter->_newheaders, $filter->_newbody]
                } else {
                        $consume = ['PRISTINE_HEADERS', $filter->_newbody];
                }
        } elsif (-f $self->filter->_newheaders) {
                $consume = [$filter->_newheaders, $filter->inputmsg];
        } else {
                $consume = [$filter->inputmsg];
        }

        foreach my $file (@$consume) {
                # Add a blank line between files
                if ($done_one) {
                        $signer->PRINT("\015\012");
                }

                if (!open($fh, '<', $file)) {
                        $self->filter->pop_tag($ctx);
                        return;
                }
                # If the file is input_msg and we've done
                # one, skip the headers
                if ($file eq $filter->inputmsg && $done_one) {
                        while (my $line = <$fh>) {
                                chomp($line);
                                last if ($line eq '');
                        }
                }

                $self->_consume($signer, $fh);
                $fh->close();
                $done_one = 1;
        }
        $signer->CLOSE();
        my $sig = $signer->signature()->as_string();
        $self->filter->pop_tag($ctx);
	$sig =~ s/^DKIM-Signature:\s+//i;
        # Remove trailing CR and LF
        chomp($sig);
        $sig =~ s/\n+$//;
        $sig =~ s/\r+$//;

        # Remove embedded CRs.  The Milter documentation says:
        # https://www.ibm.com/docs/hu/aix/7.1?topic=functions-smfi-addheader-function
        # The name or the value of the header is not checked for
        # standards compliance. However, each line of the header must
        # be below 998 characters. If you require longer header names,
        # use a multi-line header. If you must create a multi-line
        # header, insert a line feed (ASCII 0x0a, or \n in C
        # programming language) followed by a whitespace character
        # such as a space (ASCII 0x20) or tab (ASCII 0x09, or \t in C
        # programming language). The line feed cannot be preceded by a
        # carriage return (ASCII 0x0d). The mail transfer agent (MTA)
        # adds it automatically. The filter writers responsibility
        # must ensure that no standards are violated.
        $sig =~ s/\r//g;
	return $ctx->action_add_header('DKIM-Signature', $sig);
}

sub _consume
{
        my ($self, $signer, $fh) = @_;
        while(<$fh>) {
                chomp;
                s/\015$//;
                $signer->PRINT("$_\015\012");
        }
        $fh->close();
}

1;

__END__

=head1 NAME

Mailmunge::Action::DKIMSigner - Add a DKIM-Signature: header to a message

=head1 ABSTRACT

This class implements a method that adds a DKIM signature to a message.

=head1 SYNOPSIS

    package MyFilter;
    use base qw(Mailmunge::Filter);
    use Mail::DKIM::Signer;
    use Mail::DKIM::TextWrap;
    use Mailmunge::Action::DKIMSigner;

    sub filter_wrapup {
        my ($self, $ctx) = @_;
        my $signer = Mail::DKIM::Signer->new(
            Algorithm => 'rsa-sha256',
            Method    => 'relaxed/relaxed',
            Domain    => 'example.org',
            Selector  => 'my_selector',
            Key       => Mail::DKIM::PrivateKey->load(Data => get_my_key()));

        my $action = Mailmunge::Action::DKIMSigner->new($self);
        $action->add_dkim_signature($ctx, $signer);
    }

=head1 METHODS

=head2 add_dkim_signature($ctx, $signer)

Given a Mail::DKIM::Signer instance (that the caller must create with
appropriate settings), this method adds a DKIM-Signature: header to
the current message.  It should be called from filter_wrapup.

=head1 INBOUND vs OUTBOUND MAIL

Generally, we only want to sign outbound mail, so the question becomes:
How do we distinguish "outbound" from "inbound" mail?  There's no easy
answer to this because it's really a policy decision.  There are three
types of email:

=over 4

=item Inbound mail

Mail that originates from an external machine and
is destined for either the local host or a downstream SMTP server that
we control.

=item Outbound mail

Mail that originates from the local host or an
internal machine that we control and is destined for an SMTP server
that we do not control.

=item Local mail

Mail that both originates on and is destined for the
localhost or a machine that we control.

=back

One clear sign of outbound mail is mail sent from an authenticated
session.  You can detect this by looking at
C<$ctx-E<gt>mta_macro('auth_authen')>; if this is defined and
non-blank, then the SMTP session is authenticated.

Otherwise, you can obtain the connecting SMTP client address from
C<$ctx-E<gt>connecting_ip>, and for each recipient, you can examine
the destination mailer with C<$ctx-E<gt>get_recipient_mailer($rcpt)>.
These should give you enough information to determine if the
originating machine and destination machine(s) are local or off-site.

=head1 WARNING

C<Mailmunge::Action::DKIMSigner> can correctly sign a message that has
not been modified, or whose message body has been replaced without
altering the MIME type.  However, if you have modified I<headers> that
are part of the DKIM signature, you I<must> use the "relaxed"
canonicalization for the header hash.  This is because the (new) headers
that are passed to the DKIM signer might be folded differently from the
headers that actually go out on the wire.

Obviously, if you modify any headers I<after> signing the message,
you may well break the signature.  So adding the DKIM signature
should generally be the very last thing you do in C<filter_wrapup>.
