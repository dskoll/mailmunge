/***********************************************************************
*
* read_nointr.c
*
* Implementation of wrapper aound "read" that restarts system call
* if it fails due to EINTR
*
* Copyright (C) 2022 by Dianne Skoll
* https://www.mailmunge.org/
*
* This program may be distributed under the terms of the GNU General
* Public License, Version 2,
*
* SPDX-License-Identifier: GPL-2.0-only
*
***********************************************************************/
#include <unistd.h>
#include <errno.h>

/* read_nointr: Wrapper around read(2), but we automatically retry
   the system call if it fails with EINTR */
ssize_t
read_nointr(int fd, void *buf, size_t count)
{
    ssize_t n;
    while(1) {
        n = read(fd, buf, count);
        if (n < 0) {
            if (errno == EINTR) {
                continue;
            }
        }
        return n;
    }
}

